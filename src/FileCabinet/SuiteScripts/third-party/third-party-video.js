/**
 * Prompts the user if the current project has not been re-baselined in some time
 *
 * @copyright 2020 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 *
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope Public
 * @NAmdConfig ./amdconfig.json
 * @appliedtorecord job
 */
define(["moment"], (moment) => {
  const message = "Project has not been re-baselined in over two months.";

  function pageInit(context) {
    let lastBaseline = moment(context.currentRecord.getValue({fieldId: "lastbaselinedate"}));

	if (lastBaseline.isValid() && moment().diff(lastBaseline, "months") >= 2) {
	  alert(message);
	}
  }

  return { pageInit };
});
