/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 */
define(
  ['N/error', 'N/https', './new-data', './new-ui'],
  (error, https, data, ui) => {
    const onRequest = (context) => {
      log.audit({ title: `${context.request.method} request received` })

      const route = ({
        [https.Method.GET]: onGet,
        [https.Method.POST]: onPost
      })[context.request.method] ?? onError

      route?.(context)
    }

    const onGet = (context) => {
      context.response.writePage({ pageObject: ui.generateForm() })
    }

    const onPost = (context) => {
      const zip = context?.request?.parameters?.custpage_zipcode
      const pageObject = ui.populateResults(ui.generateForm(), data.findCustomersByZip(zip))

      context.response.writePage({ pageObject })
    }

    const onError = (context) => {
      throw error.create({
        name: 'INVALID_REQUEST',
        message: 'Invalid request. See cause.request property for details.',
        request: context.request
      })
    }

    return { onRequest }
  }
)
