/**
 * @NApiVersion 2.1
 */
define(['N/ui/serverWidget'], (ui) => {
  const generateForm = () => {
    const formDefinition = {
      submit: {
        label: 'Search'
      },
      fields: [
        // [id, type, label, Function(Field):void]
        ['custpage_zipcode', ui.FieldType.TEXT, 'Enter Customer ZIP Code', zipField]
      ],
      sublists: [
        // [id, type, label, columns]
        [
          'custpage_results', ui.SublistType.LIST, 'Customer Search Results',
          [
            // [id, type, label]
            ['custpage_billaddr1', ui.FieldType.TEXT, 'Billing Address Line 1'],
            ['custpage_billaddressee', ui.FieldType.TEXT, 'Billing Addressee'],
            ['custpage_billattention', ui.FieldType.TEXT, 'Billing Attention'],
            ['custpage_billcity', ui.FieldType.TEXT, 'Billing City'],
            ['custpage_billcountry', ui.FieldType.TEXT, 'Billing Country'],
            ['custpage_billstate', ui.FieldType.TEXT, 'Billing State'],
            ['custpage_billzipcode', ui.FieldType.TEXT, 'Billing ZIP Code'],
            ['custpage_currid', ui.FieldType.TEXT, 'Current ID']
          ]
        ]
      ]
    }

    const form = ui.createForm({ title: 'Customer ZIP Code Search Tool' })

    form.addSubmitButton(formDefinition.submit)

    formDefinition.fields.forEach(([id, type, label, decorate]) => {
      const field = form.addField({ id, type, label })
      decorate?.(field)
    })
    formDefinition.sublists.forEach(([id, type, label, columns]) => {
      const sublist = form.addSublist({ id, type, label })
      columns.forEach(([id, type, label]) => sublist.addField({ id, type, label }))
    })

    return form
  }

  const zipField = (field) => {
    field.setHelpText('Enter 5-digit ZIP code to find exact or nearby customers')
    field.maxLength = 5
  }

  const populateResults = (form, results) => {
    const sublist = form.getSublist({ id: 'custpage_results' })

    results.forEach((result, line) =>
      Object.entries(result).forEach(([id, value]) =>
        sublist.setSublistValue({ id, line, value: value ?? 'N/A' }))
    )

    return form
  }

  return { generateForm, populateResults }
})
