/**
 * @NApiVersion 2.1
 */
define(['N/error', 'N/query'], (error, query) => {
  const findCustomersByZip = (zipCode) => {
    if (!zipCode) {
      throw error.create({
        name: 'INVALID_ZIP',
        message: 'zipCode is required'
      })
    }

    return query.runSuiteQL({
      query: `
        SELECT
          addr.addr1 as custpage_billaddr1,
          addr.addressee as custpage_billaddressee,
          addr.attention as custpage_billattention,
          addr.city as custpage_billcity,
          addr.country as custpage_billcountry,
          addr.state as custpage_billstate,
          addr.zip as custpage_billzipcode,
          c.id as custpage_currid
        FROM
          EntityAddress as addr
        JOIN
          customer as c
        ON
          c.defaultbillingaddress = addr.nkey
        WHERE 
          (
            addr.zip = '${zipCode}' OR 
            addr.zip LIKE '${zipCode.substring(0, 3)}%'
          ) AND
          isinactive = 'F'
      `,
    }).asMappedResults()
  }

  return { findCustomersByZip }
})
