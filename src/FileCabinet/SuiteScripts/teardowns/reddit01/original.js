/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope Public
 * @see https://www.reddit.com/r/SuiteScript/comments/1icd4f2/not_getting_results_on_custom_suitescript/
 */
define(['N/ui/serverWidget', 'N/search', 'N/redirect', 'N/runtime', 'N/log'],
  function(serverWidget, search, redirect, runtime, log) {

    function onRequest(context) {
      if (context.request.method === 'GET') {
        var form = serverWidget.createForm({
          title: 'Customer ZIP Code Search Tool'
        });

        // Add ZIP code input field with helper text
        var zipField = form.addField({
          id: 'custpage_zipcode',
          type: serverWidget.FieldType.TEXT,
          label: 'Enter Customer ZIP Code'
        });
        zipField.setHelpText('Enter 5-digit ZIP code to find exact or nearby customers');
        zipField.maxLength = 5;

        // Create results sublist
        var resultList = form.addSublist({
          id: 'custpage_results',
          type: serverWidget.SublistType.LIST,
          label: 'Customer Search Results'
        });

        // Add relevant customer information columns
        resultList.addField({
          id: 'custpage_billaddr1',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing Address Line 1'
        });
        resultList.addField({
          id: 'custpage_billaddressee',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing Addressee'
        });
        resultList.addField({
          id: 'custpage_billattention',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing Attention'
        });
        resultList.addField({
          id: 'custpage_billcity',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing City'
        });
        resultList.addField({
          id: 'custpage_billcountry',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing Country'
        });
        resultList.addField({
          id: 'custpage_billstate',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing State'
        });
        resultList.addField({
          id: 'custpage_billzipcode',
          type: serverWidget.FieldType.TEXT,
          label: 'Billing ZIP Code'
        });
        resultList.addField({
          id: 'custpage_currid',
          type: serverWidget.FieldType.TEXT,
          label: 'Current ID'
        });
        resultList.addField({
          id: 'custpage_custentity_cirrus_ctype',
          type: serverWidget.FieldType.TEXT,
          label: 'Customer Type'
        });

        // Handle search if ZIP code is provided
        var searchZip = context.request.parameters.custpage_zipcode;
        if (searchZip) {
          performSearch(searchZip, resultList);
        }

        form.addSubmitButton('Search');
        context.response.writePage(form);
      } else {
        var scriptObj = runtime.getCurrentScript();
        var scriptId = scriptObj.id;
        var deploymentId = scriptObj.deploymentId;
        var zipcode = context.request.parameters.custpage_zipcode;

        // Check if scriptId, deploymentId, and zipcode are defined
        if (scriptId && deploymentId && zipcode) {
          redirect.toSuitelet({
            scriptId: scriptId,
            deploymentId: deploymentId,
            parameters: {
              'zipcode': zipcode
            }
          });
        } else {
          log.error('Redirect Error', 'Missing scriptId, deploymentId, or zipcode parameter');
        }
      }
    }

    function performSearch(zipCode, resultList) {
      try {
        // First attempt: Exact ZIP code match
        var searchResults = search.create({
          type: search.Type.CUSTOMER,
          filters: [
            ['billzipcode', 'is', zipCode],
            'AND',
            ['isinactive', 'is', 'F']
          ],
          columns: [
            'address',
            'addressee',
            'attention',
            'city',
            'country',
            'state',
            'billzipcode',
            'id',
            'custentity_cirrus_ctype'
          ]
        }).run().getRange(0, 1000);

        log.debug('Search Results', JSON.stringify(searchResults));

        // If no exact matches, search for nearby using ZIP prefix
        if (searchResults.length === 0) {
          var zipPrefix = zipCode.substring(0, 3);
          searchResults = search.create({
            type: search.Type.CUSTOMER,
            filters: [
              ['billzipcode', 'startswith', zipPrefix],
              'AND',
              ['isinactive', 'is', 'F']
            ],
            columns: [
              'address',
              'addressee',
              'attention',
              'city',
              'country',
              'state',
              'billzipcode',
              'id',
              'custentity_cirrus_ctype'
            ]
          }).run().getRange(0, 1000);

          log.debug('Search Results (Nearby)', JSON.stringify(searchResults));
        }

        // Display results
        for (var i = 0; i < searchResults.length; i++) {
          var result = searchResults[i];
          log.debug('Result ' + i, JSON.stringify(result));

          // Ensure each field exists before setting it in the sublist
          if (result && result.getValue('address')) {
            resultList.setSublistValue({
              id: 'custpage_billaddr1',
              line: i,
              value: result.getValue('billaddr1') || 'N/A'
            });
          }
          if (result && result.getValue('addressee')) {
            resultList.setSublistValue({
              id: 'custpage_billaddressee',
              line: i,
              value: result.getValue('billaddressee') || 'N/A'
            });
          }
          if (result && result.getValue('attention')) {
            resultList.setSublistValue({
              id: 'custpage_billattention',
              line: i,
              value: result.getValue('billattention') || 'N/A'
            });
          }
          if (result && result.getValue('city')) {
            resultList.setSublistValue({
              id: 'custpage_billcity',
              line: i,
              value: result.getValue('billcity') || 'N/A'
            });
          }
          if (result && result.getValue('country')) {
            resultList.setSublistValue({
              id: 'custpage_billcountry',
              line: i,
              value: result.getValue('billcountry') || 'N/A'
            });
          }
          if (result && result.getValue('state')) {
            resultList.setSublistValue({
              id: 'custpage_billstate',
              line: i,
              value: result.getValue('billstate') || 'N/A'
            });
          }
          if (result && result.getValue('billzipcode')) {
            resultList.setSublistValue({
              id: 'custpage_billzipcode',
              line: i,
              value: result.getValue('billzipcode') || 'N/A'
            });
          }
          if (result && result.getValue('id')) {
            resultList.setSublistValue({
              id: 'custpage_currid',
              line: i,
              value: result.getValue('currid') || 'N/A'
            });
          }
          if (result && result.getValue('custentity_cirrus_ctype')) {
            resultList.setSublistValue({
              id: 'custpage_custentity_cirrus_ctype',
              line: i,
              value: result.getValue('custentity_cirrus_ctype') || 'N/A'
            });
          }
        }

        // Add no results message if needed
        if (searchResults.length === 0) {
          resultList.addField({
            id: 'custpage_nomatch',
            type: serverWidget.FieldType.INLINEHTML,
            label: ' '
          }).defaultValue = '<p style="color: red">No customers found in or near this ZIP code.</p>';
        }
      } catch (e) {
        log.error('Search Error', e);
        throw e;
      }
    }

    return {
      onRequest: onRequest
    };
  });
