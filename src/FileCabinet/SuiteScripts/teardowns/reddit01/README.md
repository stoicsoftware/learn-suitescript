A refactor of a Suitelet that locates Customers by Billing Zip Code.

The 
[original code](https://www.reddit.com/r/SuiteScript/comments/1icd4f2/not_getting_results_on_custom_suitescript/) was posted by 
`u/Rapid-Decay1` on `r/SuiteScript` and is contained in the `original.js` file.

My refactored approach is contained within the `refactor/` directory.

### Summary of Changes / Observations

* Decomposed the Suitelet into multiple modules: 
  the Suitelet entry point (`new-sl.js`), UI generation (`new-ui.js`), and
  data retrieval (`new-data.js`).
* Used a declarative approach to defining the `Form`; this cuts down on the
  verbosity and repetitiveness of NetSuite's `serverWidget` API.
* Used a SuiteQL Query instead of a Search.
* Combined the "fuzzy" matches with the exact matches in the results.
* Removed the "No Results" message since the `Sublist` component already 
  provides one when empty.
* Removed the Redirection mechanic; didn't seem necessary.
* Removed the `custpage_custentity_cirrus_ctype` field/column simply because
  I do not have that field in my account.
