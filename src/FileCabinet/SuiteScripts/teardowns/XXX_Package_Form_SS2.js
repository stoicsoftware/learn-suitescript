/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 *@NModuleScope SameAccount
 *@contents suitelet-form
 */
define(["N/search", "N/ui/serverWidget", "N/url", "N/record", "N/log"], function (s, ui, url, rec, log) {
    function onRequest(context) {
        var shipparentId = context.request.parameters.reqURL;
        if (context.request.method === 'GET') {
            var form = ui.createForm({
                title: 'Shipment Package Form'
            });
            var fldgrp1 = form.addFieldGroup({
                id: 'fldgrp1',
                label: 'Primary Info'
            });
            var fldgrp2 = form.addFieldGroup({
                id: 'fldgrp2',
                label: 'Dimensions'
            });
            var fldgrp3 = form.addFieldGroup({
                id: 'fldgrp3',
                label: 'Submit'
            });
            var headfld = form.addField({
                id: 'custrecord_xxx_shipment_parent',
                type: ui.FieldType.TEXT,
                label: 'Parent Shipment ID'
            });
            headfld.updateDisplayType({
                displayType: ui.FieldDisplayType.INLINE
            });
            headfld.defaultValue = shipparentId;
            var fld1 = form.addField({
                id: 'custrecord_xxx_pkgnumber',
                type: ui.FieldType.TEXT,
                label: 'Package Number',
                container: 'fldgrp1'
            });
            var fld2 = form.addField({
                id: 'custrecord_xxx_piece_count',
                type: ui.FieldType.TEXT,
                label: 'Piece Count',
                container: 'fldgrp1'
            });
            var fld3 = form.addField({
                id: 'custrecord_xxx_packagetype',
                type: ui.FieldType.SELECT,
                label: 'Package Type',
                container: 'fldgrp1'
            });
            fld3.addSelectOption({
                value: '1',
                text: 'Box'
            });
            fld3.addSelectOption({
                value: '2',
                text: 'Bundle'
            });
            fld3.addSelectOption({
                value: '3',
                text: 'Carton'
            });
            fld3.addSelectOption({
                value: '4',
                text: 'Piece'
            });
            fld3.addSelectOption({
                value: '5',
                text: 'Skid'
            });
            var fld4 = form.addField({
                id: 'custrecord_xxx_inventoryunits',
                type: ui.FieldType.TEXT,
                label: 'Inventory Count',
                container: 'fldgrp1'
            });
            var fld5 = form.addField({
                id: 'custrecord_xxx_nmfc_number_line',
                type: ui.FieldType.SELECT,
                label: 'NMFC',
                container: 'fldgrp1'
            });
            fld5.addSelectOption({
                value: '1',
                text: 'Hand Tools'
            });
            fld5.addSelectOption({
                value: '2',
                text: 'Mortar Boxes (steel & poly mortar)'
            });
            fld5.addSelectOption({
                value: '3',
                text: 'Mortar Hoes'
            });
            fld5.addSelectOption({
                value: '4',
                text: 'Mortar Stands'
            });
            fld5.addSelectOption({
                value: '5',
                text: 'Scrapers'
            });
            fld5.addSelectOption({
                value: '6',
                text: 'Straightedges'
            });
            fld5.addSelectOption({
                value: '7',
                text: 'Screeds'
            });
            fld5.addSelectOption({
                value: '8',
                text: 'Concrete Tampers'
            });
            fld5.addSelectOption({
                value: '9',
                text: 'Dirt Tampers'
            });
            fld5.addSelectOption({
                value: '10',
                text: 'Spreaders (901,902)(917,903)'
            });
            fld5.addSelectOption({
                value: '11',
                text: 'Spreaders (904,905)(964,965)(944,945)'
            });
            fld5.addSelectOption({
                value: '12',
                text: 'Concrete Chutes'
            });
            fld5.addSelectOption({
                value: '13',
                text: 'Lutes or Rakes'
            });
            fld5.addSelectOption({
                value: '14',
                text: 'Drywall Dollys'
            });
            fld5.addSelectOption({
                value: '15',
                text: 'Featheredges'
            });
            fld5.addSelectOption({
                value: '16',
                text: 'Texture Units'
            });
            fld5.addSelectOption({
                value: '17',
                text: 'Handles'
            });
            fld5.addSelectOption({
                value: '18',
                text: 'Rebar Cutter Bender'
            });
            fld5.addSelectOption({
                value: '19',
                text: 'Blades/Float Pans'
            });
            var fld6 = form.addField({
                id: 'custrecord_xxx_hazmat',
                type: ui.FieldType.CHECKBOX,
                label: 'Hazmat',
                container: 'fldgrp1'
            });
            var fld7 = form.addField({
                id: 'custrecord_xxx_weight',
                type: ui.FieldType.TEXT,
                label: 'Weight',
                container: 'fldgrp2'
            });
            var fld8 = form.addField({
                id: 'custrecord_xxx_length',
                type: ui.FieldType.TEXT,
                label: 'Length',
                container: 'fldgrp2'
            });
            var fld9 = form.addField({
                id: 'custrecord_xxx_width',
                type: ui.FieldType.TEXT,
                label: 'Width',
                container: 'fldgrp2'
            });
            var fld10 = form.addField({
                id: 'custrecord_xxx_height',
                type: ui.FieldType.TEXT,
                label: 'Height',
                container: 'fldgrp2'
            });
            form.addSubmitButton({
                label: 'Submit',
                container: 'fldgrp3'
            });
            context.response.writePage(form);
        } else {
            var pkgnum = context.request.parameters.custrecord_xxx_pkgnumber;
            var piececnt = context.request.parameters.custrecord_xxx_piece_count;
            var pkgtype = context.request.parameters.custrecord_xxx_packagetype;
            var invunits = context.request.parameters.custrecord_xxx_inventoryunits;
            var nmfcId = context.request.parameters.custrecord_xxx_nmfc_number_line;
            var weight = context.request.parameters.custrecord_xxx_weight;
            var length = context.request.parameters.custrecord_xxx_length;
            var width = context.request.parameters.custrecord_xxx_width;
            var height = context.request.parameters.custrecord_xxx_height;
            var hazmat = context.request.parameters.custrecord_xxx_hazmat;
            var shparentId = context.request.parameters.custrecord_xxx_shipment_parent;
            var recObj = rec.create({
                type: 'customrecord_xxx_shipment_line'
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_pkgnumber',
                value: pkgnum
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_piece_count',
                value: piececnt
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_packagetype',
                value: pkgtype
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_inventoryunits',
                value: invunits
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_nmfc_number_line',
                value: nmfcId
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_weight',
                value: weight
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_length',
                value: length
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_width',
                value: width
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_height',
                value: height
            });
            recObj.setValue({
                fieldId: 'custrecord_xxx_shipment_parent',
                value: shparentId
            });
            var packageId = recObj.save({});
            context.response.write('Package Created');
        }
    }

    return {
        onRequest: onRequest
    };
});