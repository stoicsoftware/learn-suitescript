The examples presented in this folder are my versions of NetSuite's
[official SuiteScript examples](https://github.com/oracle-samples/netsuite-suitecloud-samples).

Neither repository is "more correct" than the other. Rather, these are intended
to be
used as a learning tool.

Use the corresponding examples presented in the two repositories to compare
different approaches to the same problem. Compare my approach to theirs;
incorporate ideas from both into your own
toolbox for solving NetSuite problems with SuiteScript.
