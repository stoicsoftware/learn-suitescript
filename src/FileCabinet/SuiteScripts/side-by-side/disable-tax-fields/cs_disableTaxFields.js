/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['./manageTaxFields'], (taxManager) => {
  /**
   * @param {PageInitContext} context
   */
  const pageInit = (context) => {
    taxManager.manageEditableFields(context)
  }

  return { pageInit }
})
