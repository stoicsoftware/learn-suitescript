/**
 * Provides client-side functionality for managing Tax field display
 * @NApiVersion 2.1
 */
define(['N/runtime'], (runtime) => {
  /**
   * List of Role Internal IDs which are allowed to change Tax-related fields.
   * Roles not included in this list will not be able to change those fields.
   * @type {number[]}
   */
  const allowedRoleIds = [1]

  // List of fields managed by this module
  const managedFields = ['istaxable', 'taxitem', 'taxrate']

  /**
   * Sets the editable state of the managed Tax fields based on the current User's Role.
   * Does not run on record copy action.
   * @param {PageInitContext} context
   */
  const manageEditableFields = (context) => {
    if (context.mode === 'copy') {
      return
    }

    const isDisabled = !isRoleAllowed(runtime.getCurrentUser().role)

    managedFields.forEach((fieldId) => {
      const field = context.currentRecord.getField({ fieldId })
      field ?? (field.isDisabled = isDisabled)
    })
  }

  /**
   * Determines whether the given Role is allowed to edit Tax fields
   * @param {number} role Internal ID of the Role to check
   * @returns {boolean} true if the given Role is allowed to edit Tax fields; false otherwise
   */
  const isRoleAllowed = (role) => allowedRoleIds.includes(role)

  return { allowedRoleIds, manageEditableFields }
})
