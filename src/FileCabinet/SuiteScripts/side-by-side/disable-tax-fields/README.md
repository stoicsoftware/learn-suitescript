## Original

You can find the original NetSuite example
[here](https://github.com/oracle-samples/netsuite-suitecloud-samples/blob/main/disable-tax-fields/src/FileCabinet/SuiteScripts/cs_disableTaxFields.js).

## Notable Changes and Reasoning

* Refactored to manage the Allowed Role list from within the code rather than
  from Script Deployments. This sacrifices the ability for non-technical users
  to manage the list in favor of managing the list from one central place. When
  the list is managed across multiple Deployments, whenever it changes, you need
  to remember to change every Deployment the same way. We can mitigate that risk
  with this approach, but it also has drawbacks.
* Removed `try..catch` as we don't gain much from logging and re-throwing the
  error. In a Client Script, letting a thrown error bubble up will display an 
  alert and log the error in the console for further investigation.
* Extracted tax field management into a separate module. This functionality
  seems likely to be applied to records with existing larger Client Scripts.
  Due to NetSuite's hard limit of 10 Client Scripts per record type, we
  probably want to incorporate this functionality into those existing Scripts
  rather than maintaining an additional Script. By extracting into a custom
  module, we can reuse this functionality across any number of Client Scripts
  without increasing our Client Script count. This module also becomes
  expandable with future capabilities as well.
* Removed alias variables for `context` values.
* Reversed `copy` condition to exit early; I prefer my functions to detect
  exit conditions and exit as early as possible.
* Removed `NSUtil` methods in favor of similar native JS functionality.
  `isempty()` is replaced by a 
  [falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy) 
  evaluation, and `inArray()` is replaced by
  [Array.includes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes)
* Added `PageInitContext` typedef.

## Other Suggestions

* Currently the Role list is an Array of 
  [magic numbers](https://en.wikipedia.org/wiki/Magic_number_(programming)).
  Consider adding an enumeration of these values to be more programmatically
  descriptive of each value.
* The `manageEditableFields` function is tightly coupled to the `pageInit`
  event. In Production code, we would likely want to abstract this coupling
  and ensure we could execute this function from multiple entry points (e.g.
  a User Event `beforeLoad`).
