/**
 * Descriptor for the context object passed in to the `beforeSubmit` entry point
 * @typedef {Object} BeforeSubmitContext
 * @param {string} type Type of operation invoked by the event
 * @param {Record} newRecord The new record being loaded
 * @param {Record} oldRecord The old record (previous state of the record) in read-only mode
 */

/**
 * Descriptor for the context object passed in to the `beforeLoad` entry point
 * @typedef {Object} BeforeLoadContext
 * @property {Record} newRecord The new record being loaded
 * @property {Form} form The current form
 * @property {string} type The type of operation invoked by the event (the trigger type)
 */

/**
 * Sales Order data required for calculating Commissions
 * @typedef {Object} CommissionData
 * @property {number} subtotal The Order subtotal
 * @property {CommissionItem[]} items List of Items
 */

/**
 * Line-level Sales Order data required for calculating Commissions
 * @typedef {Object} CommissionItem
 * @property {number} msrp The Item's MSRP amount
 * @property {number} quantity
 * @property {number} type
 */

/**
 * Descriptor for the context object passed in to the `fieldChanged` entry point
 * @typedef {Object} FieldChangedContext
 * @property {string} fieldId The ID of the field which changed.
 * @property {string} sublistId The ID of the sublist which changed.
 * @property {Record} currentRecord The current form record.
 */

/**
 * Descriptor for the context object passed in to the `pageInit` entry point
 * @typedef {Object} PageInitContext
 * @property {CurrentRecord} currentRecord The current form record
 * @property {string} mode The mode in which the record is being accessed.
 */
