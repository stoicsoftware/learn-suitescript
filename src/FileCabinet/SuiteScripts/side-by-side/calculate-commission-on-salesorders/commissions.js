/**
 * @NApiVersion 2.1
 */
define([], () => {
  // Item lines with an itemtype present in this list will *not* be counted when calculating
  // Commissions
  const ignoredItemTypes = ['Discount', 'Markup', 'Subtotal']

  // The multiplicative factor used when calculating the Net Distributor Cost for the Order
  const netDistributorCostFactor = 0.5

  /**
   * Calculates the total Commission value for the given CommissionData
   * @param {CommissionData} data
   * @returns {number} Commission total
   */
  const calculate = (data) => {
    const msrpTotal = calculateMsrp(data)
    const netDistributorCost = msrpTotal * netDistributorCostFactor

    /*
     In Production code, this calculation would deserve a detailed description of the
     business rules and scenarios that drive the calculation. Since that level of documentation
     was not included in the original example, any reasons I invent here would be thin and
     contrived.

     The specifics of such a calculation aren't as pertinent to learning SuiteScript, which
     is the intent of these examples.
     */
    return (netDistributorCost / 10) +
      Math.min(data.subtotal, msrpTotal) * 0.75 +
      Math.max(data.subtotal - msrpTotal, 0) / 2
  }

  /**
   * Transforms the given Sales Order `Record` instance into `CommissionData`
   * @param {Record} order
   * @returns {CommissionData}
   */
  const fromSalesOrder = (order) => {
    const itemCount = order.getLineCount({ sublistId: 'item' })
    const subtotal = parseFloat(order.getValue({ fieldId: 'subtotal' })) || 0

    /** @type CommissionData */
    const commissionData = { items: [], subtotal }

    for (let line = 0; line < itemCount; line++) {
      const itemLine = { sublistId: 'item', line }

      const msrp = parseFloat(order.getSublistValue({ fieldId: 'custcol_salesorder_msrp', ...itemLine })) || 0
      const quantity = parseFloat(order.getSublistValue({ fieldId: 'quantity', ...itemLine })) || 0
      const type = order.getSublistValue({ fieldId: 'itemtype', ...itemLine })

      commissionData.items.push({ msrp, quantity, type })
    }

    return commissionData
  }

  /**
   * Calculates the MSRP total for the given CommissionData
   * @param {CommissionData} data
   * @returns {number} MSRP total
   */
  const calculateMsrp = (data) => data.items
    .filter(item => !ignoredItemTypes.includes(item.type))
    .reduce((sum, item) => (sum + item.msrp * item.quantity), 0)

  return { calculate, fromSalesOrder }
})
