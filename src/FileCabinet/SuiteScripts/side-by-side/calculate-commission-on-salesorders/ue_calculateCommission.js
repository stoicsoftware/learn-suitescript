/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['./commissions'], (commissions) => {
  /**
   * Calculates Commission values for the Sales Order
   * @param {BeforeSubmitContext} context
   */
  const beforeSubmit = (context) => {
    if (![context.UserEventType.CREATE, context.UserEventType.EDIT].includes(context.type)) {
      return
    }

    const commissionData = commissions.fromSalesOrder(context.newRecord)
    const commissionTotal = commissions.calculate(commissionData)
    context.newRecord.setValue({ fieldId: 'custbody_commission_amount', value: commissionTotal })
  }

  return { beforeSubmit }
})
