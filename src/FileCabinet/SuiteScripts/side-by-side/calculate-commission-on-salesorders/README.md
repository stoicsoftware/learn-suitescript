## Original

You can find the original NetSuite example
[here](https://github.com/oracle-samples/netsuite-suitecloud-samples/blob/main/calculate-commission-on-salesorders/src/FileCabinet/SuiteScripts/ue_calculateCommission.js).

## Notable Changes and Reasoning:

* Moved calculation to `beforeSubmit` event since we do not have obvious
  external dependencies and are updating the record in context; typically we use
  `afterSubmit` to update _other_ records. This may or may not be "correct" in
  any given account as it greatly depends on the other customizations involved
  and how the Commissions value is calculated.
* Added formal typedefs for `CommissionData` and `CommissionItem`. By
  parsing the data from the `Record` instance into plain JavaScript Objects and
  primitives, we can normalize the data before we operate on it. This results in
  more consistent behavior, simpler error handling, and more portable code
  should we ever need to repeat it in a new entry point. It also makes future
  unit tests simpler because we eliminate the need to mock NetSuite methods.
* Extracted commissions calculation logic into its own custom module for
  isolation and potential portability later. We often need to calculate
  Commissions from a variety of entry points, not just in a single User Event.
* Removed the `try..catch` as it did not seem worthwhile to catch and re-throw
  an error only to change the error title in a log message. Other examples
  dedicated to error handling will be better suited to showcase strategies
  for routing and manipulating errors.
* Changed `* 0.1` to `/ 10` since `0.1` can
  [cause rounding problems](https://medium.com/better-programming/why-is-0-1-0-2-not-equal-to-0-3-in-most-programming-languages-99432310d476)
  in IEEE floating point arithmetic.
* Added formal typedef for `BeforeSubmitContext`
  in [typedefs.js](../typedefs.js).
* Removed now-unused `N/error` and `N/record` imports.
* Removed explicit import of `N/log`; it is implicit in all SuiteScript modules.

## Other Suggestions

* Unit tests on the `calculate` method of the `commissions.js` file could be
  used to validate Commissions calculations for various business situations.
  These are not included here (yet) as I don't have good definitions of the
  business cases driving the calculation. Any unit tests I wrote would be
  tests of the current implementation only, not tests of the actual business
  use cases. Good unit tests are implementation-agnostic.
