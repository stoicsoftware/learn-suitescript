/**
 * @NApiVersion 2.1
 * @NScriptType UserEvent
 * @NModuleScope SameAccount
 */
define(['N/runtime'], (runtime) => {
  /**
   * When appropriate, renders the "Open Suitelet" button on the Form
   * @param {BeforeLoadContext} context
   */
  const beforeLoad = (context) => {
    if (needsButton(context)) {
      renderButton(context.form)
    }
  }

  /**
   * Determines whether the given Record is in the Pending Fulfillment status
   * @param {Record} rec The Record instance to evaluate
   * @returns {boolean} true if the given Record is in the Pending Fulfillment status; false
   *   otherwise.
   */
  const isPendingFulfillment = (rec) => {
    const status = rec.getValue({ fieldId: 'status' })
    return status === 'Pending Fulfillment'
  }

  /**
   * Determines whether the script is running in the User Interface context
   * @returns {boolean} true if the current context is the UI; false otherwise
   */
  const isUserInterface = () => runtime.executionContext === runtime.ContextType.USER_INTERFACE

  /**
   * Determines whether the given context is valid for rendering the Suitelet button
   * @param context
   * @returns {boolean}
   */
  const needsButton = (context) => [
    isUserInterface(),
    isPendingFulfillment(context),
    targetUrl()
  ].every(Boolean)

  /**
   * Renders the Suitelet button to the given Form
   * @param {Form} form
   */
  const renderButton = (form) => form.addButton({
    id: 'custpage_suiteletbutton',
    label: 'Open Suitelet',
    functionName: `window.open("${targetUrl()}")`
  })

  /**
   * Retrieves the URL to open when clicking the button
   * @returns {string} the target URL
   */
  const targetUrl = () => runtime.getCurrentScript()
    .getParameter({ name: 'custscript_suiteletlink' })

  return { beforeLoad }
})
