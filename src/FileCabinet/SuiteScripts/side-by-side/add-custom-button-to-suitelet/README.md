## Original

You can find the original NetSuite example
[here](https://github.com/oracle-samples/netsuite-suitecloud-samples/blob/main/add-custom-button-to-suitelet/src/FileCabinet/SuiteScripts/sl_executeSuiteletButton.js).

## Notable Changes and Reasoning:

* Eliminated `try..catch` in order to avoid silent failures. I want to _know_ 
  when something goes wrong; only logging a message is too passive for me.
* Extracted button addition and condition detection into functions. 
  Function names read more naturally to me than raw procedural code; I prefer
  my entry point functions to read almost like natural language.
* Changed filename as this is a User Event, not a Suitelet.
* Removed comments that re-state what the line below does. Comments are
  [not the place](https://stoic.software/articles/jsdoc-patterns/) 
  I teach code literacy.
* Added path for handling an undefined target URL.
* Added check for User Interface context; no need to render a button if there's
  no UI to render on.
* Replaced URL string concatenation with template literal; easier to read and
  does not require dancing around multiple quote styles.
* Removed explicit `N/log` import; it is implicit in all SuiteScript modules.
* Removed alias variables for `context` values; personal preference.
* Added formal typedef for `BeforeLoadContext` in [typedefs.js](../typedefs.js)
* Reformatted using StandardJS and ES2021; personal styling preference.

## Other Suggestions

* Rather than relying on a Script Parameter to store the URL,
  `N/url.resolveScript()` might be used to retrieve the appropriate URL
  dynamically.
* Instead of using an interpreted inline `functionName`, consider adding or 
  updating a Client Script to handle the button click instead.
