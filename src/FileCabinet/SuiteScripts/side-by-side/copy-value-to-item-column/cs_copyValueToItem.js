/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/search'], (search) => {
  /**
   * `fieldChanged` entry point handler
   * @param {FieldChangedContext} context
   */
  const fieldChanged = (context) => {
    // Routes to appropriate handling function based on field ID
    const route = ({
      custcol_billingitem: onBillingItemChange
    })[context.fieldId] ?? noop

    route?.(context)
  }

  /**
   * Retrieve an Item ID from its UPC code
   * @param {string} upc
   * @returns {?number} Internal ID of the corresponding Item record, if found; undefined
   *   otherwise
   */
  const findItemByUpc = (upc) => {
    if (!upc) {
      return
    }

    return search.create({
      type: search.Type.NON_INVENTORY_ITEM,
      filters: [
        ['upccode', search.Operator.IS, upc]
      ]
    }).run().getRange({ start: 0, end: 1 })
      ?.[0]?.id
  }

  /**
   * `custcol_billingitem` change handler.
   *
   * Sets the `item` column to the Item record which corresponds to the UPC code set in
   * `custcol_billingitem`.
   *
   * @param {FieldChangedContext} context
   */
  const onBillingItemChange = (context) => {
    const upc = context.currentRecord.getCurrentSublistText({
      sublistId: 'item',
      fieldId: 'custcol_billingitem'
    })

    const itemId = findItemByUpc(upc)

    if (itemId) {
      context.currentRecord.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        value: itemId
      })
    }
  }

  // No Operation for intentionally unhandled field changes
  const noop = () => { /* Intentionally empty */ }

  return { fieldChanged }
})
