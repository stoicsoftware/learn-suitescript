## Original

You can find the original NetSuite example
[here](https://github.com/oracle-samples/netsuite-suitecloud-samples/blob/main/copy-value-to-item-column/src/FileCabinet/SuiteScripts/cs_copyValueToItem.js).

## Notable Changes and Reasoning

* Removed alias variables for `context` values; personal preference I avoid
  doing this unless the values are deeply nested in a layered Object.
* Implemented [router pattern](https://stoic.software/articles/22-event-router/)
  for handling field changes; this is a consistent pattern I use for entry
  points which are frequently overloaded with multiple paths.
* Extracted search to separate function and added handling for empty UPC;
  removed unused search columns and switched raw values to search module enums.
* Refactored to set `item` column by ID instead of by text; I prefer using
  internal IDs where possible instead of names as names can often change.
* Added `FieldChangedContext` typedef

## Other Suggestions

* The `route` structure gets re-created every time a field changes. This could
  instead be extracted out to a module-level constant to avoid that overhead. In
  a larger Client Script, that constant might evolve into a larger structure
  containing other routes for other entry points. 
