/**
 * @typedef {object} AdminRequest
 * @property {string} action
 * @property {object} params
 */

/**
 * @typedef {[string, string, string, object]} AdminRequestLinkTuple
 * @example
 * [
 *   string: Text label for the link,
 *   string: `id` value for the link,
 *   string: `data-action` value to identify which request to perform
 *   object: `data-params` default value for data to be sent with the request
 * ]
 * @example
 * ['Get Entries', 'btn-get-entries', 'getEntryList', { pageSize: 2 }]
 */



/**
 * @typedef {object} AirtableEntry
 * @property {string} [name]
 * @property {string} [airtableId]
 * @property {string} [netsuiteId]
 */

/**
 * Body options that can be provided for multi-record Airtable API requests
 * @typedef {object} AirtableListOptions
 * @property {string[]} [fields] List of fields to include in each record in the response
 * @property {string} [filterByFormula]
 *   {@link https://support.airtable.com/docs/formula-field-reference|formula} to filter
 *   responses
 * @property {number} [maxRecords]
 * @property {number} [pageSize]
 */

/**
 * Body format used for single-record Airtable API requests
 * @typedef {object} AirtableRecord
 * @property {object} fields key-value pairs containing the record data
 * @property {string} id Airtable unique ID for the record
 */

/**
 * Body format used for multi-record Airtable API requests
 * @typedef {object} AirtableRecords
 * @property {AirtableRecord[]} records
 */

/**
 * @typedef {object} AirtableRequestBase
 * @property {string} baseId
 * @property {string} tableId
 */

/**
 * @typedef {object} AirtableRequest
 * @property {object} [body] data sent in the body of a POST or PUT request
 * @property {object} [params] key-pairs used for URL parameters of a GET request
 * @property {string} [recordId] Airtable ID of an individual record
 */

/**
 * @typedef {object} AirtableResponse
 * @property {object} body
 * @property {number} code HTTP response code
 */

/**
 * @typedef {object} AirtableUserId
 * @property {string} id
 * @property {string} [email]
 * @see https://airtable.com/developers/web/api/get-user-id-scopes
 */

/**
 * Tuple to be passed to `Sublist.addField`
 * @typedef {[string, FieldType, string, function(field):void]} ColumnDescriptor
 * @example
 * [
 *   string: fieldId,
 *   FieldType: type,
 *   string: label,
 *   function: called after the Field is created to modify the properties of the Field
 * ]
 * @example
 * ['custpage_col_request', ui.FieldType.TEXT, 'Request', (field) => field.defaultValue='TEST']
 * @see {Field}
 * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_456415649413.html
 */

/**
 * Tuple to be passed to `Form.addField`
 * @typedef {[string, serverWidget.FieldType, string, string, function(Field):void]} FieldDescriptor
 * @example
 * [
 *   string, // fieldId
 *   FieldType, // type
 *   string, // label
 *   string, // container
 *   function // called after the Field is created to modify the properties of the Field
 * ]
 * @example
 * [
 *   'custpage_f_response',
 *   ui.FieldType.LONGTEXT,
 *   'Response',
 *   'custpage_g_output',
 *   (field) => field.updateDisplayType({ displayType: ui.FieldDisplayType.INLINE }
 * ]
 * @see {Field}
 * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4337905245.html
 */

/**
 * Tuple to be passed to `Form.addFieldGroup`
 * @typedef {[string, string, function(FieldGroup):void]} FieldGroupDescriptor
 * @example
 * [
 *   string, // id
 *   string, // label
 *   function // called after the Field Group is created to modify the properties of the Field Group
 * ]
 * @example
 * ['custpage_g_output', 'Response Data', (group) => group.isCollapsible = true]
 * @see {FieldGroup}
 * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4337960739.html
 */

/**
 * Tuple to be passed to `Form.addSublist`
 * @typedef {[string, SublistType, string, ColumnDescriptor[]]} SublistDescriptor
 * @example
 * [
 *   string, // sublistId
 *   SublistType, // type
 *   string, // label
 *   ColumnDescriptor[] // List of Columns to add to the Sublist
 * ]
 * @example
 * [
 *   'custpage_list_requests', ui.SublistType.LIST, 'Airtable Requests',
 *   [
 *     ['custpage_col_request', ui.FieldType.TEXT, 'Request', (field) => field.defaultValue='TEST'],
 *   ]
 * ]
 * @see {Sublist}
 * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4339569512.html
 */

/**
 * @typedef {object} OnRequestContext
 * @property {ServerRequest} request
 * @property {ServerResponse} response
 */

/**
 * Describes a mapping between corresponding NetSuite and Airtable fields
 * @typedef {[string, string, boolean]} TransformDescriptor
 * @example
 * [
 *   string, // Airtable ID for the field
 *   string, // NetSuite ID for the field
 *   boolean // `true` if the Airtable field is a computed/formula field; `false` otherwise
 * ]
 * @example
 * ['fldKs3oeU75nNfn6D', 'externalid', true]
 */
