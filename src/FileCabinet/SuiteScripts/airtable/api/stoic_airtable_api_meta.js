/**
 * stoic_airtable_api_meta.js
 *
 * Interface for working with Airtable metadata
 *
 * @module stoic_airtable_api_meta
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(['./stoic_airtable_RequestBuilder'], (builder) => {
  // Static Airtable identifiers
  // @see https://airtable.com/developers/web/api/get-user-id-scopes
  const airtableConfig = { baseId: 'meta', tableId: 'whoami' }
  let whoamiRequest

  /**
   * Retrieves the ID of the authenticated Airtable user
   * @gov 10
   * @returns {AirtableUserId}
   */
  const whoami = () => {
    whoamiRequest ??= builder.createGet({ ...airtableConfig })
    return whoamiRequest()
  }

  return /** @alias module:stoic_airtable_api_meta */ {
    whoami
  }
})
