/**
 * stoic_airtable_api_entries.js
 *
 * Interface for working with Airtable Entries custom table
 *
 * @module stoic_airtable_api_entries
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([
  './stoic_airtable_RequestBuilder',
  './stoic_airtable_api_enum',
  '/SuiteScripts/airtable/transform/stoic_airtable_tf_entries'
], (builder, enums, transform) => {
  // Static Airtable identifiers
  // @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries
  const airtableConfig = {
    baseId: 'appyME6WGNnpn0Vwd',
    tableId: 'tblmXj7YmnGS3O0XZ'
  }

  // Caches for request memoization
  let deleteRequest, getRequest, postRequest, putRequest

  /**
   * Delete a single Airtable Entry record
   * @param {AirtableEntry} entry The Entry to delete
   * @returns {AirtableRecord}
   * @throws {ReferenceError} if no Entry is provided
   * @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries:delete
   */
  const _delete = (entry) => {
    if (!entry) {
      throw ReferenceError(`No Entry provided to delete().`)
    }

    deleteRequest ??= builder.createDelete({ ...airtableConfig })

    const request = { recordId: entry.airtableId }
    return deleteRequest(request)
  }

  /**
   * Retrieve a single Airtable Entry record
   * @param {AirtableEntry} entry The Entry to retrieve
   * @returns {AirtableResponse}
   * @throws {ReferenceError} if no Entry is provided
   * @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries:retrieve
   */
  const get = (entry) => {
    if (!entry) {
      throw ReferenceError(`No Entry provided to get(). Did you intend to call getList() instead?`)
    }

    getRequest ??= builder.createGet({ ...airtableConfig })
    const request = {
      recordId: entry.airtableId,
      params: enums.defaultOptions.get
    }
    const response = getRequest(request)
    return transform.from.response(response)
  }

  /**
   * Retrieve a list of Airtable Entry records
   * @param {AirtableListOptions} [options]
   * @returns {AirtableEntry[]}
   * @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries:list
   */
  const getList = (options) => {
    postRequest ??= builder.createPost({ ...airtableConfig })

    const request = {
      // FIXME Abusing the current design here; not the intended use, but functional
      recordId: 'listRecords',
      body: { ...enums.defaultOptions.get, ...options }
    }
    const response = postRequest(request)
    return transform.from.responseList(response)
  }

  /**
   * Update a single Airtable Entry record
   * @param {AirtableEntry} entry The Entry to update
   * @returns {AirtableRecord}
   * @throws {ReferenceError} if no Entry is provided
   * @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries:update
   */
  const update = (entry) => {
    if (!entry) {
      throw new ReferenceError(`No Entry provided to update().`)
    }

    putRequest ??= builder.createPut({ ...airtableConfig })

    const request = {
      recordId: entry.airtableId,
      body: {
        ...enums.defaultOptions.put,
        fields: onlyStaticFields(entry).fields
      }
    }
    const response = putRequest(request)
    return transform.from.response(response)
  }

  /**
   * Update a list of Airtable Entry records
   * @param {AirtableEntry[]} entries The Entries to update
   * @returns {AirtableRecord[]}
   * @throws {ReferenceError} if no Entry is provided
   * @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries:update
   */
  const upsertList = (entries) => {
    if (!entries?.length) {
      throw new ReferenceError(`No Entries provided to updateList().`)
    }

    putRequest ??= builder.createPut({ ...airtableConfig })

    const request = {
      body: {
        ...enums.defaultOptions.put,
        // FIXME Don't like this module needing any understanding of the underlying field ID
        performUpsert: { fieldsToMergeOn: ['flde2ZxfLH9VcLLrR'] },
        records: entries.map(onlyStaticFields)
      }
    }

    const response = putRequest(request)
    return transform.from.response(response)
  }

  /**
   * Prepares the given AirtableEntry for an upsert request by removing computed fields
   * @gov 0
   * @param {AirtableEntry} entry
   * @returns {AirtableRecord}
   */
  const onlyStaticFields = (entry) => [
    transform.without.computed,
    transform.to.requestRecord
  ].reduce((newEntry, fn) => fn(newEntry), entry)

  return /** @alias module:stoic_airtable_api_meta */ {
    delete: _delete,
    get,
    getList,
    update,
    upsertList
  }
})
