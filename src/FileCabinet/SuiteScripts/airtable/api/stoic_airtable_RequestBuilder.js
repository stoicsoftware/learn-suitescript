/**
 * stoic_airtable_RequestBuilder.js
 *
 * Utility module for building REST API requests that align with the Airtable API
 *
 * @module stoic_airtable_RequestBuilder
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 * @see {@link https://airtable.com/developers/web/api/introduction|Airtable API Docs}
 */
define(['./stoic_airtable_api_enum', 'N/https', 'N/url'], (enums, https, url) => {

  /**
   * Partially applied function to generate an Airtable DELETE request
   * @gov 0
   * @param {AirtableRequestBase} requestBase
   * @returns {function(AirtableRequest): AirtableResponse}
   */
  const createDelete = (requestBase) => {
    /**
     * Sends an HTTP DELETE request to Airtable
     * @gov 10
     * @param [request] {AirtableRequest} Airtable Request parameters
     * @returns {object} parsed response body
     * @throws {Error} in the event of a failed request
     */
    return ((requestData = {}) => {
      const requestUrl = generateUrl({ ...requestBase, ...requestData })
      let response = https.delete({
        url: requestUrl,
        headers: {
          'Authorization': https.createSecureString({ input: 'Bearer {custsecret_airtable}' })
        }
      })

      if (response.code !== 200) {
        const cause = { request: requestData, response, requestUrl }
        log.error({ title: 'Airtable request failure', details: cause })
        throw Error('Airtable request failure', { cause })
      }

      return JSON.parse(response.body)
    })
  }

  /**
   * Partially applied function to generate an Airtable GET request
   * @gov 0
   * @param {AirtableRequestBase} requestBase
   * @returns {function(AirtableRequest): AirtableResponse}
   */
  const createGet = (requestBase) => {
    /**
     * Sends an HTTP GET request to Airtable
     * @gov 10
     * @param [request] {AirtableRequest} Airtable Request parameters
     * @returns {object} parsed response body
     * @throws {Error} in the event of a failed request
     */
    return ((requestData = {}) => {
      const requestUrl = generateUrl({ ...requestBase, ...requestData })
      let response = https.get({
        url: requestUrl,
        headers: {
          'Authorization': https.createSecureString({ input: 'Bearer {custsecret_airtable}' })
        }
      })

      if (response.code !== 200) {
        const cause = { request: requestData, response, requestUrl }
        log.error({ title: 'Airtable request failure', details: cause })
        throw Error('Airtable request failure', { cause })
      }

      return JSON.parse(response.body)
    })
  }

  /**
   * Partially applied function to generate an Airtable POST request
   * @gov 0
   * @param {AirtableRequestBase} requestBase
   * @returns {function(AirtableRequest): AirtableResponse}
   */
  const createPost = (requestBase) => {
    /**
     * Sends an HTTP POST request to Airtable
     * @gov 10
     * @param {AirtableRequest} [request] Airtable Request parameters
     * @returns {AirtableResponse} the HTTP response
     * @function
     */
    return ((requestData) => {
      const requestUrl = generateUrl({ ...requestBase, ...requestData })
      let response = https.post({
        url: requestUrl,
        body: JSON.stringify(requestData.body),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': https.createSecureString({ input: 'Bearer {custsecret_airtable}' })
        }
      })

      if (response.code !== 200) {
        const cause = { request: requestData.body, response, requestUrl }
        log.error({ title: 'Airtable request failure', details: cause })
        throw Error('Airtable request failure', { cause })
      }

      return JSON.parse(response.body)
    })
  }

  /**
   * Partially applied function to generate an Airtable PUT request
   * @gov 0
   * @param {AirtableRequestBase} requestBase
   * @returns {function(AirtableRequest): AirtableResponse}
   */
  const createPut = (requestBase) => {
    /**
     * Sends an HTTP PUT request to Airtable
     * @gov 10
     * @param {AirtableRequest} [request] Airtable Request parameters
     * @returns {AirtableResponse} the HTTP response
     */
    return ((requestData) => {
      const requestUrl = generateUrl({ ...requestBase, ...requestData })
      let response = https.put({
        url: requestUrl,
        body: JSON.stringify(requestData.body),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': https.createSecureString({ input: 'Bearer {custsecret_airtable}' })
        }
      })

      // TODO Error format from Airtable:
      // body: { error: { message:string, type:string } }
      if (response.code !== 200) {
        const cause = { request: requestData.body, response, requestUrl }
        log.error({ title: 'Airtable request failure', details: cause })
        throw Error('Airtable request failure', { cause })
      }

      return JSON.parse(response.body)
    })
  }

  /**
   * Generates the appropriate URL for the given Airtable request
   * @gov 0
   * @param {AirtableRequest} request
   * @returns {string} Airtable request URL
   * @private
   * @example
   * generateUrl({baseId:'meta', tableId:'whoami'})
   * // <== https://api.airtable.com/v0/meta/whoami/
   * generateUrl({baseId:'appyME', tableId:'tblmXj', recordId:'rectZM'})
   * // <== https://api.airtable.com/v0/appyME/tblmXj/rectZM
   * generateUrl({baseId:'appABC', tableId:'tblXYZ', params: {pageSize:10}})
   * // <== https://api.airtable.com/v0/appABC/tblXYZ?pageSize=10
   */
  const generateUrl = (request) => {
    const u = url.format({
      domain: `https://api.airtable.com/v0/${toPath(request)}`,
      params: request.params
    })

    return encodeURI(u)
  }

  /**
   * Generate a URL path segment from a generic request
   * @gov 0
   * @param {AirtableRequest} request
   * @returns {string}
   * @example
   * toPath({baseId:'appXYZ', tableId:'tblQRS'})
   * // <== https://api.airtable.com/v0/appXYZ/tblQRS
   */
  const toPath = (request) => [
    request.baseId,
    request.tableId,
    request.recordId
  ].filter(Boolean)
    .join('/')

  return /** @alias module:stoic_airtable_RequestBuilder */ {
    createDelete,
    createGet,
    createPost,
    createPut
  }
})
