/**
 * stoic_airtable_api_enum.js
 *
 * Enumerations used by the Airtable API
 *
 * @module stoic_airtable_api_enum
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => /** @alias module:stoic_airtable_api_enum */ ({
  // Common required options for each request type
  defaultOptions: {
    get: {
      returnFieldsByFieldId: true
    },
    post: {
      returnFieldsByFieldId: true,
      typecast: true
    },
    put: {
      returnFieldsByFieldId: true,
      typecast: true
    }
  }
}))
