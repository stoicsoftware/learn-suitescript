/**
 * stoic_airtable_admin_enum.js
 *
 * Enumerations used by the Airtable Administration UI
 *
 * @module stoic_airtable_admin_enum
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => /** @alias module:stoic_airtable_admin_enum */ ({
  actions: {
    deleteEntry: 'deleteEntry',
    getEntry: 'getEntry',
    getEntryList: 'getEntryList',
    updateEntry: 'updateEntry',
    updateEntryList: 'updateEntryList',
    whoami: 'whoami'
  },
  examples: {
    entryNotSynced: {
      airtableId: 'rec8wredQL0kf1MjG',
      name: 'abc',
      netsuiteId: null
    },
    entrySynced: {
      airtableId: 'rectZMfFDzzM7bzbA',
      name: 'jkl',
      netsuiteId: 100
    },
    entriesNotSynced: [
      {
        airtableId: 'recxMUApgo8zCTxGj',
        name: 'def',
        netsuiteId: null
      },
      {
        airtableId: 'recJZ0aI4GwmfK8mo',
        name: 'ghi',
        netsuiteId: null
      }
    ],
    entriesSynced: [
      {
        airtableId: 'rec8wredQL0kf1MjG',
        name: 'abc',
        netsuiteId: 200
      },
      {
        airtableId: 'rectZMfFDzzM7bzbA',
        name: 'jkl',
        netsuiteId: 100
      }
    ]
  },
  script: {
    deploymentId: 'customdeploy_airtable_admin_sl',
    scriptId: 'customscript_airtable_admin_sl'
  }
}))
