/**
 * stoic_cl_airtable.js
 *
 * Client Script for the Airtable Suitelet
 *
 * @module stoic_cl_airtable
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 * @NScriptType ClientScript
 */
define(['./stoic_airtable_admin_enum', 'N/currentRecord', 'N/https'], (enums, cr, https) => {

  /**
   * pageInit entry point
   * @param {object} context
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410597671.html
   */
  const pageInit = (context) => {
    addHandlers()
  }

  /**
   * Adds event handlers to DOM elements
   * @gov 0
   * @private
   */
  const addHandlers = () => {
    const buttons = document.getElementById('custpage_list_requests_splits')
      .getElementsByClassName('send-request')

    for (let button of buttons) {
      button.addEventListener('click', handlers.click)
    }
  }

  // Event handlers namespace
  const handlers = {
    /**
     * Click handler for the request links
     * @gov 10
     * @param {PointerEvent} evt
     * @private
     */
    click: (evt) => {
      const params = cr.get().getSublistValue({
        sublistId: 'custpage_list_requests',
        fieldId: 'custpage_col_params',
        line: evt.target.dataset.index
      })
      const response = sendRequest(
        evt.target.dataset.action,
        params ? JSON.parse(params) : undefined
      )

      displayResponse(response)
    }
  }

  /**
   * Sends the selected HTTP request to the Admin Suitelet
   *
   * We cannot send these requests from the client-side because https.secureString
   * capabilities are server-side only, and we use secure strings to pass our
   * Secret for authentication. Instead, we send a POST request to the Admin
   * Suitelet with the relevant data so that it can send the request to
   * Airtable on our behalf.
   *
   * @gov 10
   * @param {string} action
   * @param {object} params
   * @returns {AirtableResponse}
   * @private
   */
  const sendRequest = (action, params) => https.requestSuitelet({
    ...enums.script,
    body: JSON.stringify({ action, params }),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  })

  /**
   * Update the form with the response values
   * @gov 0
   * @param {AirtableResponse} response
   * @private
   */
  const displayResponse = (response) => {
    console.dir(response)

    cr.get().setValue({ fieldId: 'custpage_f_response', value: response.body })
  }

  return /** @alias module:stoic_cl_airtable */ { pageInit }
})
