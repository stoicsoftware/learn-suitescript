/**
 * stoic_airtable_ui.js
 *
 * Presentation layer for the Airtable Suitelet
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(['./stoic_airtable_admin_enum', 'N/ui/serverWidget'], (enums, ui) => {
  /** @type {AdminRequestLinkTuple[]} */
  const requestLinks = [
    ['Who Am I?', 'btn-get-user', enums.actions.whoami],
    ['Nonsense', 'btn-oops', 'oops'],
    ['Get Entry', 'btn-get-entry', enums.actions.getEntry, enums.examples.entryNotSynced],
    ['Get Entries', 'btn-get-entries', enums.actions.getEntryList, { pageSize: 2 }],
    ['Update Entry', 'btn-update-entry', enums.actions.updateEntry, enums.examples.entrySynced],
    [
      'Update Entries',
      'btn-update-entries',
      enums.actions.updateEntryList,
      enums.examples.entriesSynced
    ],
    ['Delete Entry', 'btn-delete-entry', enums.actions.deleteEntry, enums.examples.entrySynced]
  ]

  /**
   * Renders the UI for the Airtable Administration Suitelet
   * @gov 0
   * @param {OnRequestContext} context
   * @returns {Form}
   */
  const render = (context) => {
    const form = generateForm(context)
    hydrators.requests(form)
    return form
  }

  /**
   * Creates the Form and all its UI components without any data
   * @gov 0
   * @param {OnRequestContext} context
   * @returns {Form}
   * @private
   */
  const generateForm = (context) => {
    const formDefinition = {
      /** @type {FieldGroupDescriptor[]} */
      fieldGroups: [
        ['custpage_g_output', 'Response Data', decorators.outputGroup]
      ],
      /** @type {FieldDescriptor[]} */
      fields: [
        [
          'custpage_f_response',
          ui.FieldType.LONGTEXT,
          'Response',
          'custpage_g_output',
          decorators.inlineField
        ]
      ],
      /** @type {SublistDescriptor} */
      sublists: [
        [
          'custpage_list_requests', ui.SublistType.LIST, 'Airtable Requests',
          /** @type {ColumnDescriptor[]} */
          [
            ['custpage_col_request', ui.FieldType.TEXT, 'Request', decorators.nameField],
            ['custpage_col_params', ui.FieldType.TEXTAREA, 'Query Data', decorators.paramsField]
          ]
        ]
      ]
    }

    const form = ui.createForm({ title: 'Airtable Integration Administration' })
    form.clientScriptModulePath = './stoic_airtable_admin_cl.js'

    formDefinition.fieldGroups.forEach(([id, label, decorate]) => {
      const group = form.addFieldGroup({ id, label })
      decorate?.(group)
    })
    formDefinition.fields.forEach(([id, type, label, container, decorate]) => {
      const field = form.addField({ id, type, label, container })
      decorate?.(field)
    })
    formDefinition.sublists.forEach(([id, type, label, columns]) => {
      const sublist = form.addSublist({ id, type, label })
      columns.forEach(([id, type, label, decorate]) => {
        const field = sublist.addField({ id, type, label })
        decorate?.(field)
      })
    })

    return form
  }

  // Methods for adjusting individual Field settings
  const decorators = {
    inlineField: (field) => {
      field.updateDisplayType({ displayType: ui.FieldDisplayType.INLINE })
    },
    nameField: (field) => {
      decorators.inlineField(field)
    },
    outputGroup: (group) => {
      group.isBorderHidden = false
      group.isCollapsed = false
      group.isCollapsible = true
      group.isSingleColumn = true
    },
    paramsField: (field) => {
      field.updateDisplayType({ displayType: ui.FieldDisplayType.ENTRY })
    }
  }

  // Methods for populating a sublist on the given Form with the given data
  const hydrators = {
    requests: (form) => {
      const sublist = form.getSublist({ id: 'custpage_list_requests' })

      requestLinks.forEach(([name, id, action, params], line) => {
        const button = requestButton([name, id, action, params], line)
        const paramStr = JSON.stringify(params)

        sublist.setSublistValue({ id: 'custpage_col_request', value: button, line })
        paramStr && sublist.setSublistValue({ id: 'custpage_col_params', value: paramStr, line })
      })
    }
  }

  // Translates a given Request to a button for sending the request
  const requestButton = ([name, id, action, params], i) =>
    `<a href='#' id='${id}' class='send-request' data-action='${action}' data-index='${i}'/>${name}</a>`

  return { render }
})
