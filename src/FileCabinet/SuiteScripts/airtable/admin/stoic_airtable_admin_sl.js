/**
 * stoic_sl_airtable.js
 *
 * Suitelet entry point for testing Airtable integration requests
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 * @NScriptType Suitelet
 */
define(['./stoic_airtable_admin_enum', 'N/https'], (enums, https) => {
  /**
   * Suitelet entry point
   * @param {OnRequestContext} context
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4407987288.html
   */
  const onRequest = (context) => {
    log.audit({ title: `${context.request.method} request received` })

    const route = ({
      [https.Method.GET]: onGet,
      [https.Method.POST]: onPost
    })[context.request.method] ?? onError

    route?.(context)

    log.audit({ title: 'Request complete.' })
  }

  /**
   * GET handler
   * @gov 0
   * @param {OnRequestContext} context
   * @private
   */
  const onGet = (context) => {
    let ui
    require(['/SuiteScripts/airtable/admin/stoic_airtable_admin_ui'], (m) => { ui = m })
    context.response.writePage({ pageObject: ui.render(context) })
  }

  /**
   * POST handler
   * @gov 0
   * @param {OnRequestContext} context
   * @private
   */
  const onPost = (context) => {
    /** @type {{action:string, params:object}} */
    const request = JSON.parse(context.request.body)

    // Route the incoming action parameter to an API module and method to invoke
    const [filename, method] = ({
      [enums.actions.deleteEntry]: ['entries', 'delete'],
      [enums.actions.getEntry]: ['entries', 'get'],
      [enums.actions.getEntryList]: ['entries', 'getList'],
      [enums.actions.updateEntry]: ['entries', 'update'],
      [enums.actions.updateEntryList]: ['entries', 'upsertList'],
      [enums.actions.whoami]: ['meta', 'whoami']
    })[request.action] || []

    if (!(filename && method)) {
      log.error({ title: 'Invalid route', details: request })
      return onError(context)
    }

    try {
      const output = invokeApiModule(request.params, filename, method)
      context.response.write({ output: JSON.stringify(output) })
    } catch (e) {
      log.error({ title: 'Invalid module invocation', details: { request, filename, method } })
      onError(context)
    }
  }

  /**
   * Error handler
   * @gov 0
   * @param {OnRequestContext} context
   * @private
   */
  const onError = (context) => {
    const e = {
      code: 400,
      body: context.request.body,
      method: context.request.method
    }
    log.error({ title: 'Invalid request', details: e })
    context.response.write({ output: JSON.stringify(e) })
  }

  /**
   * Invokes the module
   * @gov 0
   * @param {object} params The input data for the method that will be called
   * @param {string} name The name of the API module file, without the `stoic_airtable_api_` prefix
   * @param {string} method The name of the method to call within the API module
   * @returns {AirtableResponse}
   * @private
   */
  const invokeApiModule = (params, name, method) => {
    let output
    require([`/SuiteScripts/airtable/api/stoic_airtable_api_${name}`], (m) => {
      output = m[method]?.(params)
    })
    return output
  }

  return /** @alias module:stoic_sl_airtable */ { onRequest }
})
