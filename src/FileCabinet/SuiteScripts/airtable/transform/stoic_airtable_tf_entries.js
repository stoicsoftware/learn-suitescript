/**
 * stoic_airtable_tf_entries.js
 *
 * Transforms Airtable Entry data between NetSuite and Airtable formats
 *
 * @module stoic_airtable_tf_entries
 *
 * @copyright 2025 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => {
  // TODO Source mapping from custom record
  /**
   * Describes the mapping of corresponding NetSuite and Airtable fields to a neutral
   * data format for internal operations.
   *
   * @private
   * @see https://airtable.com/appyME6WGNnpn0Vwd/api/docs#curl/table:airtable%20entries
   */
  const mapping = {
    recordType: 'customrecord_airtable',
    fields: {
      /** @type {TransformDescriptor} */
      airtableId: ['fldKs3oeU75nNfn6D', 'externalid', true],
      /** @type {TransformDescriptor} */
      name: ['fldyzblBDE7ndV6Z7', 'name'],
      /** @type {TransformDescriptor} */
      netsuiteId: ['flde2ZxfLH9VcLLrR', 'internalid']
    },
    /*
     The Airtable API throws an error on any PUT/PATCH/POST requests that contain values
     for computed fields, so we provide these views of the mapping data for convenience
     */
    views: {
      /**
       * View of only the computed fields in the mapping
       * @gov 0
       * @returns {string[]} List of the keys which represent computed fields
       */
      get computedFields () {
        return Object.entries(mapping.fields)
          .filter(([, [, , isComputed]]) => !isComputed)
          .map(([key]) => key)
      },
      // Only the non-computed fields in the mapping
      get staticFields () {
        return Object.entries(mapping.fields)
          .filter(([, [, , isComputed]]) => !isComputed)
          .map(([key]) => key)
      }
    }
  }

  // Transforms FROM external formats TO AirtableEntry
  const from = {
    // For use with record.Record instances
    netsuiteRecord: (rec) => Object.entries(mapping.fields)
      .reduce((entry, [key, [, fieldId]]) => {
        entry[key] = rec.getValue({ fieldId })
        return entry
      }, {}), // For use with single-record Airtable API requests
    response: (responseData) => Object.entries(mapping.fields)
      .reduce((entry, [key, [airtableKey]]) => {
        entry[key] = responseData.fields[airtableKey]
        return entry
      }, {}), // For use with multi-record Airtable API requests
    responseList: (responseData) => responseData.records.map(from.response)
  }

  // Transforms FROM AirtableEntry TO external formats
  const to = {
    /**
     * Transforms the given AirtableEntry to an object which can be consumed by
     * `N/record.submitFields()`.
     *
     * @gov 0
     * @param {AirtableEntry} entry
     * @returns {{id: string, values: {}}}
     * @throws {ReferenceError} when no `netsuiteId` is given
     * @example
     * const recordValues = to.inlineValues({ airtableId: 'abc', name: 'test', netsuiteId: 123 })
     * // <== { id: 123, values: { externalid: 'abc', name: 'test', internalid: 123 } }
     * record.submitFields({ ...recordValues, type: 'customrecord_airtable' })
     */
    inlineValues: (entry) => {
      if (!entry.netsuiteId) {
        throw ReferenceError('No netsuiteId provided to inlineValues().')
      }

      const scaffold = {
        id: entry.netsuiteId,
        type: mapping.recordType,
        values: {}
      }

      return Object.entries(entry)
        .filter(([key]) => (key !== 'netsuiteId'))
        .reduce((result, [key, value]) => {
          const [, netsuiteId] = mapping.fields[key]
          value ??= '' // Overwrite nullish with empty string for clearing record values
          result.values[netsuiteId] = value
          return result
        }, scaffold)
    },
    // For use with record.Record instances
    netsuiteRecord: (entry, rec) => Object.entries(entry)
      .forEach(([key, value]) => {
        const [, fieldId] = mapping.fields[key]
        rec.setValue({ fieldId, value })
      }),
    /**
     * For use with single-record Airtable API requests
     * @gov 0
     * @param {AirtableEntry} entry
     * @returns {AirtableRecord}
     * @example
     * to.requestRecord({ name: 'abc', id: '123' })
     * // <== { id: 'rec123', fields: { 'fldyzb': 'abc', 'fldKsq' : '123' } }
     */
    requestRecord: (entry) => Object.entries(entry)
      .reduce((request, [key, value]) => {
        const [airtableId] = mapping.fields[key]
        request.fields[airtableId] = value
        return request
      }, { id: entry.airtableId, fields: {} }),
    /**
     * For use with multi-record Airtable API requests
     * @gov 0
     * @param {AirtableEntry[]} entries
     * @returns {AirtableRecords}
     * @example
     * to.requestList([{ name: 'abc', id: '123' }, { name: 'def', id: '456' }])
     * // <== { records: [
     * //  { id: 'rec123', fields: { 'fldyzb': 'abc', 'fldKsq' : '123' } }
     * //  { id: 'rec456', fields: { 'fldyzb': 'def', 'fldKsq' : '456' } }
     * //] }
     */
    requestRecords: (entries) => ({
      records: entries.map(to.requestRecord)
    })
  }

  const without = {
    /**
     * Removes computed field properties from the given AirtableEntry by
     * picking only the static field properties into a new instance
     * @gov 0
     * @param {AirtableEntry} entry
     * @returns {AirtableEntry}
     * @example
     * without.computed({ name: 'abc', airtableId: '123' })
     * // <== { 'name': 'abc' }
     */
    computed: (entry) => Object.entries(entry ?? {})
      .reduce((newEntry, [key, value]) => {
        if (mapping.views.staticFields.includes(key)) {
          newEntry[key] = value
        }
        return newEntry
      }, {})
  }

  return /** @alias module:stoic_airtable_tf_entries */ { from, to, without }
})
