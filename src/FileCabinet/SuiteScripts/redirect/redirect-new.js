/**
 * Redirects to a new record rather than an existing one using a specific Custom Form
 *
 * @copyright 2018 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 * @appliedtorecord employee
 */
define(['N/record', 'N/redirect'], function (r, redirect) {
  // The custom form that will be rendered upon redirection
  const FORM_ID = 231

  /**
   * `beforeLoad` event handler
   *
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4407991781.html
   */
  const beforeLoad = (context) => {
    if (!needsRedirect(context)) {
      return
    }

    redirect.toRecord({
      type: r.Type.EMPLOYEE,
      id: '', // Empty string here should redirect to new record; null/undefined do not work,
      parameters: { cf: FORM_ID }
    })
  }

  const needsRedirect = (context) => !([
      context.UserEventType.VIEW,
      context.UserEventType.DELETE
    ].includes(context.type))

  return { beforeLoad }
})
