define([], function () {

    /**
     * Demonstrates reading Subrecords on the server-side
     *
     * @exports ess-readsubrecord-ue
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NScriptType UserEventScript
     */
    var exports = {};

    function beforeLoad(context) {
        log.audit({title: "Entered beforeLoad..."});

        try {
            var shipAddress = context.newRecord.getSubrecord({fieldId: "shippingaddress"});
            var addressText = shipAddress.getValue({fieldId: "addrtext"});
            var country = shipAddress.getText({fieldId: "country"});
            var city = shipAddress.getValue({fieldId: "city"});

            log.debug({title: "Shipping Address:", details: addressText});
            log.debug({title: "Shipping Country:", details: country});
            log.debug({title: "Shipping City:", details: city});
        } catch (e) {
            log.error({title: "Somehow this is broken", details: e.toString()});
        }
    }

    exports.beforeLoad = beforeLoad;
    return exports;
});
