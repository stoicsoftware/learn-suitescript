/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define([], () => {
  const beforeLoad = (context) => {
    context.form.addButton({
      id: 'custpage_btn_example',
      label: 'Example Button',
      functionName: 'clickMe'
    })
    context.form.clientScriptModulePath = 'SuiteScripts/add-button/cl_add_button.js'
  }

  return { beforeLoad }
})
