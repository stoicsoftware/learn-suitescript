/**
 * @NAmdConfig /SuiteScripts/add-button/amdconfig.json
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 */
define(['lib/common'], (common) => {
  const clickMe = (evt) => {
    console.info('clicked')
    common.sampleFunction()
  }
  return {
    pageInit: () => {},
    clickMe
  }
})
