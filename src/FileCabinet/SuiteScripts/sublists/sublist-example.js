/**
 An example User Event script that shows how to work with sublists.

 Whenever a Sales Order that contains item *LAM00002* is created, a new line
 for item *LED00001* should also be added.

 - The Quantity of the two lines should be identical.
 - The Price for line item *LED00001* should be `0`
 - Only applies if `custbody_replace_order` (a checkbox) is unchecked.
 - Only applies if the Sales Order Department is *Service* (Internal ID = 3)
 */

var _stoic = _stoic || {};

_stoic.sublist = (function () {

    /**
     * Automatically adds the LED00001 line to appropriate Sales Orders
     *
     * @exports stoic/sublist/example
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh
     */
    var exports = {};

    /**
     * The internal ID of the Lamp item that makes an order eligible for the
     * LED Promotion
     *
     * @type {String}
     *
     * @private
     * @member lampId
     */
    var lampId = "253";

    /**
     * The internal ID of the LED item for the LED Promotion
     *
     * @type {String}
     *
     * @private
     * @member ledId
     */
    var ledId = "691";

    /**
     * Enumeration of the Departments
     *
     * @enum
     * @type {Number}
     *
     * @private
     * @member departments
     */
    var departments = {
        "ADMIN": 1,
        "SALES": 2,
        "SERVICE": 3,
        "MARKETING": 4,
        "PRODUCTION": 5,
        "MACHINE": 6,
        "ASSEMBLY": 7,
        "INSPECTION": 9,
        "FABRICATION": 10,
        "ENGINEERING": 11
    };

    /**
     * Event fired just before a database write operation
     *
     * @appliedtorecord salesorder
     *
     * @governance 0
     *
     * @param {String} type Operation types:
     * <ul>
     * <li>create</li>
     * <li>edit</li>
     * <li>delete</li>
     * <li>xedit</li>
     * <li>approve</li>
     * <li>cancel</li>
     * <li>reject (SO, ER, Time Bill, PO & RMA only)</li>
     * <li>pack</li>
     * <li>ship (IF only)</li>
     * <li>markcomplete (Call, Task)</li>
     * <li>reassign (Case)</li>
     * <li>editforecast (Opp, Estimate)</li>
     * </ul>
     *
     * @return {void}
     *
     * @static
     * @function beforeSubmit
     */
    function beforeSubmit(type){
        ledPromotion(nlapiGetNewRecord());
    }

    /**
     * Entry point for the LED Promotion engine
     *
     * @governance 0
     *
     * @param order {nlobjRecord} The Sales Order record to process
     *
     * @return {void}
     *
     * @private
     * @function ledPromotion
     */
    function ledPromotion(order) {
        nlapiLogExecution("AUDIT", "LED", "Running LED Promotion engine...");

        if (!orderIsEligible(order)) {
            nlapiLogExecution("AUDIT", "LED", "Order not eligible.");
            return;
        }

        addLed(order);
    }

    /**
     * Determines whether the given Sales Order record is eligible for
     * the LED promotion
     *
     * @governance 0
     *
     * @param order {nlobjRecord} The Sales Order record to process
     *
     * @return {Boolean}
     *
     * @private
     * @function orderIsEligible
     */
    function orderIsEligible(order) {
        return (
            hasEligibleDepartment(order) &&
            !isReplacementOrder(order) &&
            containsEligibleItem(order)
        );
    }

    /**
     * Determines whether the given Sales Order record is in an appropriate
     * Department for the LED Promotion
     *
     * @governance 0
     *
     * @param order {nlobjRecord} The Sales Order record to check Department
     *
     * @return {Boolean} true if the order is in an eligible Department; false
     *     otherwise
     *
     * @private
     * @function hasEligibleDepartment
     */
    function hasEligibleDepartment(order) {
        return (order.getFieldValue("department") == departments.SERVICE);
    }

    /**
     * Determines whether the given Sales Order record is a replacement order.
     * Replacement orders are not eligible for the LED Promotion.
     *
     * @governance 0
     *
     * @param order {nlobjRecord} The Sales Order record to check
     *
     * @return {Boolean} true if the order is a replacement order; false
     *     otherwise
     *
     * @private
     * @function isReplacementOrder
     */
    function isReplacementOrder(order) {
        return (order.getFieldValue("custbody_replace_order") == "T");
    }

    /**
     * Determines whether the given Sales Order contains an appropriate item
     * that makes it eligible for the LED Promotion
     *
     * @governance 0
     *
     * @param order {nlobjRecord} The Sales Order record to check
     *
     * @return {Boolean} true if the order contains an eligible item; false
     *     otherwise
     *
     * @private
     * @function containsEligibleItem
     */
    function containsEligibleItem(order) {
        var lampIndex = order.findLineItemValue("item", "item", lampId);
        nlapiLogExecution("DEBUG", "LED", lampIndex);
        return (lampIndex > 0);
    }

    function addLed(order) {
        nlapiLogExecution("AUDIT", "LED", "Adding LED promotional item...");

        var lampIndex = order.findLineItemValue("item", "item", lampId);
        var lampQuantity = order.getLineItemValue("item", "quantity", lampIndex);

        order.selectNewLineItem("item");
        order.setCurrentLineItemValue("item", "item", ledId);
        order.setCurrentLineItemValue("item", "quantity", lampQuantity);
        order.setCurrentLineItemValue("item", "rate", 0);
        order.commitLineItem("item");
    }

    exports.beforeSubmit = beforeSubmit;
    return exports;
})();
