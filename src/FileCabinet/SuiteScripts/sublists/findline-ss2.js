// Finding a specific line item in SuiteScript 2.0...

require(["N/record"], function (r) {
    var rec = r.load({
        "type": r.Type.SALES_ORDER,
        "id": 123
    });

    // Find the line that contains item 777
    var index = rec.findSublistLineWithValue({"sublistId": "item", "fieldId": "item", "value": 777});

    // find returns -1 if the item isn't found
    if (index > -1) {
        // we found it on line "index"
    } else {
        // item 777 is not in the list
    }
});
