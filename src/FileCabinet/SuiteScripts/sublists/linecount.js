// How many Items does a Sales Order have...

// ... if we're in the context of a Sales Order record
var itemCount = nlapiGetLineItemCount("item");

// ... or if we've loaded the Sales Order
var order = nlapiLoadRecord("salesorder", 123);
var itemCount = order.getLineItemCount("item");