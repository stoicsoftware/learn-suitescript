// Inserting a line item at the beginning of a sublist in Standard Mode...

// ... if the record is in context:
nlapiInsertLineItem("item", 1);
nlapiSetLineItemValue("item", "item", 1, 456);
nlapiSetLineItemValue("item", "quantity", 1, 10);

// ... or if we have a reference to the record (rec):
rec.insertLineItem("item", 1);
rec.setLineItemValue("item", "item", 1, 456);
rec.setLineItemValue("item", "quantity", 1, 10);

// Inserting a line item before the last item of a sublist in Standard Mode...

// ... if the record is in context:
var itemCount = nlapiGetLineItemCount("item");
nlapiInsertLineItem("item", itemCount);
nlapiSetLineItemValue("item", "item", itemCount, 456);
nlapiSetLineItemValue("item", "quantity", itemCount, 10);

// ... or if we have a reference to the record (rec):
var itemCount = rec.getLineItemCount("item");
rec.insertLineItem("item", itemCount);
rec.setLineItemValue("item", "item", itemCount, 456);
rec.setLineItemValue("item", "quantity", itemCount, 10);