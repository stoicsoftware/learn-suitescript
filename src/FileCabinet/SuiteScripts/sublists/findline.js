// Which line has item 456 on it...

// ... if we're in the context of a record
var index = nlapiFindLineItemValue("item", "item", 456);
if (index > -1) {
    // we found it...
} else {
    // item 456 is not in the list
}

// ... or if we have a reference to the record (rec)
var index = rec.findLineItemValue("item", "item", 456);
if (index > -1) {
    // we found it on line "index"...
} else {
    // item 456 is not in the list
}
