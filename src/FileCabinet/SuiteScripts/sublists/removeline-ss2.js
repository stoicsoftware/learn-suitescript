// Removing line items in SuiteScript 2.0...

require(["N/record"], function (r) {
    var rec = r.load({
        "type": r.Type.SALES_ORDER,
        "id": 123
    });

    // Remove the first line
    rec.removeLine({"sublistId": "item", "line": 0});

    // Remove the last line
    rec.removeLine({"sublistId": "item", "line": rec.getLineCount({"sublistId": "item"}) - 1});

    rec.save();
});
