/**
 * Sends a message to the console for every Entry Point so that users can monitor how the events
 * are fired as they interact with the record in the NetSuite UI.
 *
 * @copyright 2017 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType ClientScript
 */
define([], () => {
  /**
   * `pageInit` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410597671.html
   */
  const pageInit = (context) => {
    console.info(`CS pageInit triggered: ${context.mode}`)
  }

  /**
   * `validateField` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410693152.html
   */
  const validateField = (context) => {
    console.info(`CS validateField triggered: ${context.fieldId}`)
    return true
  }

  /**
   * `fieldChanged` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410692508.html
   */
  const fieldChanged = (context) => {
    console.info(`CS fieldChanged triggered: ${context.fieldId}`)
  }

  /**
   * `postSourcing` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410692646.html
   */
  const postSourcing = (context) => {
    console.info(`CS postSourcing triggered: ${context.fieldId}`)
  }

  /**
   * `lineInit` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410693004.html
   */
  const lineInit = (context) => {
    console.info(`CS lineInit triggered: ${context.sublistId}`)
  }

  /**
   * `validateLine` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410693302.html
   */
  const validateLine = (context) => {
    console.info(`CS validateLine triggered: ${context.sublistId}`)
    return true
  }

  /**
   * `validateInsert` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410693455.html
   */
  const validateInsert = (context) => {
    console.info(`CS validateInsert triggered: ${context.sublistId}`)
    return true
  }

  /**
   * `validateDelete` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410693608.html
   */
  const validateDelete = (context) => {
    console.info(`CS validateDelete triggered: ${context.sublistId}`)
    return true
  }

  /**
   * `sublistChanged` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410692812.html
   */
  const sublistChanged = (context) => {
    console.info(`CS sublistChanged triggered: ${context.sublistId}`)
  }

  /**
   * `saveRecord` event handler
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4410693749.html
   */
  const saveRecord = (context) => {
    console.info('CS saveRecord triggered')
    return true
  }

  return {
    fieldChanged,
    lineInit,
    pageInit,
    postSourcing,
    saveRecord,
    sublistChanged,
    validateDelete,
    validateField,
    validateInsert,
    validateLine
  }
})
