/**
 * Logs a message to the Script's Execution Log on each entry point so that users can monitor how
 * the events are fired as they interact with the record.
 *
 * @copyright 2017 Stoic Software, LLC
 * @author Eric T Grubaugh <eric@stoic.software>
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 */
define([], () => {

  /**
   * `beforeLoad` event handler
   *
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4407991781.html
   */
  const beforeLoad = (context) => {
    log.audit({ title: 'UE beforeLoad triggered', details: context.type })
  }

  /**
   * `beforeSubmit` event handler
   *
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4407992070.html
   */
  const beforeSubmit = (context) => {
    log.audit({ title: 'UE beforeSubmit triggered', details: context.type })
  }

  /**
   * `afterSubmit` event handler
   *
   * @see https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4407992281.html
   */
  const afterSubmit = (context) => {
    log.audit({ title: 'UE afterSubmit triggered', details: context.type })
  }

  return { afterSubmit, beforeLoad, beforeSubmit }
})
