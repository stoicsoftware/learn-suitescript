## Learn SuiteScript

This repository contains many examples used for introducing new NetSuite
developers to SuiteScript. 

#### Helpful Resources

* Eric's [Free Stuff](https://stoic.software/free/)
* [NetSuite Professionals'](http://netsuiteprofessionals.com/) Slack community
* [NetSuite documentation](http://stackoverflow.com/documentation/netsuite/topics)
    on Stack Overflow
* [NetSuite Records Browser](https://system.na1.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2016_1/script/record/account.html)
    for finding record object and SOAP schema files
* [Mozilla Developer Network](https://developer.mozilla.org/) is an excellent
    JavaScript reference
