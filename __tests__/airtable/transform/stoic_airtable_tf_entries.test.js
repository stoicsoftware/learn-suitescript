const transform = require(
  '../../../src/FileCabinet/SuiteScripts/airtable/transform/stoic_airtable_tf_entries')

describe('airtable::transform::entries::', () => {
  describe('from::', () => {})
  describe('to::', () => {
    describe('inlineValues()', () => {
      it('is consumable by submitFields', () => {
        const input = {
          airtableId: 'abc',
          name: 'test',
          netsuiteId: 123
        }
        const expected = {
          id: 123,
          type: 'customrecord_airtable',
          values: {
            name: 'test',
            externalid: 'abc'
          }
        }

        const actual = transform.to.inlineValues(input)

        expect(actual).toStrictEqual(expected)
      })
      it('does not submit internalid', () => {
        const input = {
          airtableId: 'abc',
          name: 'test',
          netsuiteId: 123
        }

        const actual = transform.to.inlineValues(input)

        expect(actual).not.toHaveProperty('values.internalid')
      })
      it('submits nullish as empty string', () => {
        const input = {
          airtableId: undefined,
          name: null,
          netsuiteId: 123
        }
        const expected = {
          id: 123,
          type: 'customrecord_airtable',
          values: {
            name: '',
            externalid: ''
          }
        }

        const actual = transform.to.inlineValues(input)

        expect(actual).toStrictEqual(expected)
      })
      it('does submit empty string', () => {
        const input = {
          airtableId: 'abc',
          name: '',
          netsuiteId: 123
        }

        const actual = transform.to.inlineValues(input)

        expect(actual).toHaveProperty('values.name', '')
      })
    })
  })
  describe('without::', () => {
    describe('computed()', () => {
      it('removes computed fields', () => {
        const input = {
          airtableId: 'abc',
          name: 'test',
          netsuiteId: 123
        }
        const expected = {
          name: 'test',
          netsuiteId: 123
        }

        const actual = transform.without.computed(input)

        expect(actual).toStrictEqual(expected)
      })
      it('does not modify the original instance', () => {
        const input = {
          airtableId: 'abc',
          name: 'test',
          netsuiteId: 123
        }

        const actual = transform.without.computed(input)

        expect(input).toMatchObject(actual)
        expect(input).not.toBe(actual)
      })
      it('accepts empty input gracefully', () => {
        const inputs = [{}, null, undefined]
        const expected = [{}, {}, {}]

        const actual = inputs.map((input) => transform.without.computed(input))

        expect(actual).toMatchObject(expected)
      })
      it('returns empty object when all fields are computed fields', () => {
        const input = { airtableId: 'abc' }
        const expected = {}

        const actual = transform.without.computed(input)

        expect(actual).toStrictEqual(expected)
      })
      it('makes no changes when all fields are static fields', () => {
        const input = { name: 'abc', netsuiteId: 123 }
        const expected = { name: 'abc', netsuiteId: 123 }

        const actual = transform.without.computed(input)

        expect(actual).toStrictEqual(expected)
      })
    })
  })
})
